#include <iostream>
using namespace std;

class Stack
{
    private:
        int* Values;
        int size;
        int top_index;
    public:
        Stack(int s)
        {
            if (s > 0)
            {
                Values = new int[s];
                size = s;
                top_index = -1;
            }
        }
        void destructor()
        {
            delete[] Values;
        }
        void push(int value)
        {
            if (top_index < size)
            {
                top_index++;
                Values[top_index] = value;
            }
            else
                cout << "Stack is full!" << endl;
        }
        void pop()
        {
            if (top_index >= 0)
            {
                Values[top_index] = 0;
                top_index--;
            }
        }
        void print_stack()
        {
            for(int i = top_index; i >= 0; i--)
                cout << Values[i] << " ";
            cout << endl;
        }
};

int main()
{   
    int count = 4;
    Stack stack(count);
    for (int i = 0; i < count; i++) stack.push(i);
    stack.print_stack();
    stack.pop();
    stack.pop();
    stack.print_stack();
    stack.destructor();
    return 0;
}